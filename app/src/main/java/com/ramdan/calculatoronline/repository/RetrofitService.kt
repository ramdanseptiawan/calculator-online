package com.example.mvvmcalculator.repository

import com.ramdan.calculatoronline.Constant
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RetrofitService {

    @GET("add")
    fun getAdd(@Query("numbers") numbers: Array<Double>?) : Call<Double>

    @GET("substract")
    fun getSubstract(@Query("numbers") numbers: Array<Double>?) : Call<Double>

    @GET("multiply")
    fun getMultiply(@Query("numbers") numbers: Array<Double>?) : Call<Double>

    @GET("divide")
    fun getDivide(@Query("number1") number1: Double?, @Query("number2") number2: Double) : Call<Double>

    companion object {

        var retrofitService: RetrofitService? = null

        fun getInstance() : RetrofitService {

            if (retrofitService == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl(Constant.Base_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                retrofitService = retrofit.create(RetrofitService::class.java)
            }
            return retrofitService!!
        }
    }
}