package com.example.mvvmcalculator.repository

class MainRepository constructor(private val retrofitService: RetrofitService) {

    fun getAdd(numbers:Array<Double>?) = retrofitService.getAdd(numbers)

    fun getSubstract(numbers:Array<Double>?) = retrofitService.getSubstract(numbers)

    fun getMultiply(numbers:Array<Double>?) = retrofitService.getMultiply(numbers)

    fun getDivide(number1:Double,number2:Double) = retrofitService.getDivide(number1,number2)
}